import os
from setuptools import setup, find_packages

import warnings

from distutils.core import setup

package = __import__('faq')

try:
    README = open('README.md').read()
except:
    warnings.warn('Could not read README.md')
    README = None

try:
    REQUIREMENTS = open('requirements.txt').read()
except:
    warnings.warn('Could not read requirements.txt')
    REQUIREMENTS = None

try:
    TEST_REQUIREMENTS = open('requirements_test.txt').read()
except:
    warnings.warn('Could not read requirements_test.txt')
    TEST_REQUIREMENTS = None

setup(
    name = 'faq',
    version = '1.1.1',
    description = (
        'Web.mvc App to manage FAQs on your website.'
    ),
    long_description = README,
    
    author  ='Danny Waser',
    author_email = 'danny@waser.tech',
    url = 'https://gitlab.waser.tech/web.mvc-apps/faq',
    
    #packages = find_packages(exclude=['example']),
    packages=[
        'faq',
        'faq.migrations',
    ],
    include_package_data=True,
    zip_safe = False,
    
    classifiers = [
        'Development Status :: 3 - Alpha',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Framework :: Django',
    ],
    
    install_requires = REQUIREMENTS,
    test_suite = "faq._testrunner.runtests",
    tests_require = TEST_REQUIREMENTS,
)
