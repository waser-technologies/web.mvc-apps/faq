from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _

@apphook_pool.register
class FAQApphook(CMSApp):
    name = _("FAQ")
    app_name = "faq"
    def get_urls(self, page=None, language=None, **kwargs):
        return ["faq.urls"]       # replace this with the path to your application's URLs module