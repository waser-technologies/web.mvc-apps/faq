import datetime
from django.db import models
from django.urls import reverse
from django.utils.translation import get_language, ugettext, ugettext_lazy as _
from django.contrib.auth import get_user_model
from django.template.defaultfilters import slugify
from .managers import QuestionManager
from parler.models import TranslatableModel, TranslatedFields
from aldryn_apphooks_config.models import AppHookConfig
from aldryn_apphooks_config.fields import AppHookConfigField

User = get_user_model()

class Topic(TranslatableModel):
    """
    Generic Topics for FAQ question grouping
    """
    translations = TranslatedFields(
        name = models.CharField(_('name'), max_length=150),
        slug = models.SlugField(_('slug'), max_length=150),
    )

    sort_order = models.IntegerField(_('sort order'), default=0,
        help_text=_('The order you would like the topic to be displayed.'))

    def get_absolute_url(self, lang=None):
        if not lang or lang not in self.get_available_languages():
            lang = get_language()
        if not lang or lang not in self.get_available_languages():
            lang = self.get_current_language()
        if self.has_translation(lang):
            slug = self.safe_translation_getter('slug', language_code=lang)
            return reverse(
                'faq:faq_topic_detail',
                kwargs={'slug': slug},
                current_app="faq"
            )
        # in case topic doesn't exist in this language, gracefully fallback
        # to topic list
        return reverse(
            'faq:faq_topic_list'
        )

    class Meta:
        verbose_name = _("Topic")
        verbose_name_plural = _("Topics")
        ordering = ['sort_order']
    
    def __str__(self):
        default = ugettext('FAQ Topic (no translation)')
        return self.safe_translation_getter('name', any_language=True, default=default)


    def __unicode__(self):
        return self.name

class Question(TranslatableModel):
    HEADER = 2
    ACTIVE = 1
    INACTIVE = 0
    STATUS_CHOICES = (
        (ACTIVE,    _('Active')),
        (INACTIVE,  _('Inactive')),
        (HEADER,    _('Group Header')),
    )
    
    translations = TranslatedFields(
        text = models.TextField(_('question'), help_text=_('The actual question itself.')),
        answer = models.TextField(_('answer'), blank=True, help_text=_('The answer text.')),
        slug = models.SlugField(_('slug'), max_length=100),
    )

    topic = models.ForeignKey(Topic, verbose_name=_('topic'), related_name='questions', on_delete=models.CASCADE)
    status = models.IntegerField(_('status'),
        choices=STATUS_CHOICES, default=INACTIVE, 
        help_text=_("Only questions with their status set to 'Active' will be "
                    "displayed. Questions marked as 'Group Header' are treated "
                    "as such by views and templates that are set up to use them."))
    
    protected = models.BooleanField(_('is protected'), default=False,
        help_text=_("Set true if this question is only visible by authenticated users."))
        
    sort_order = models.IntegerField(_('sort order'), default=0,
        help_text=_('The order you would like the question to be displayed.'))

    created_on = models.DateTimeField(_('created on'), default=datetime.datetime.now)
    updated_on = models.DateTimeField(_('updated on'))
    created_by = models.ForeignKey(User, verbose_name=_('created by'),
        null=True, related_name="+", on_delete=models.SET_NULL)
    updated_by = models.ForeignKey(User, verbose_name=_('updated by'),
        null=True, related_name="+", on_delete=models.SET_NULL)
    
    objects = QuestionManager()
    
    class Meta:
        verbose_name = _("Frequent asked question")
        verbose_name_plural = _("Frequently asked questions")
        ordering = ['sort_order', 'created_on']

    def get_absolute_url(self, lang=None):
        if not lang or lang not in self.get_available_languages():
            lang = get_language()
        if not lang or lang not in self.get_available_languages():
            lang = self.get_current_language()
        if self.has_translation(lang):
            slug = self.safe_translation_getter('slug', language_code=lang)
            return reverse(
                'faq:faq_question_detail',
                kwargs={'slug': slug, 'topic_slug': self.topic.safe_translation_getter('slug', language_code=lang)},
                current_app="faq"
            )
        # in case question doesn't exist in this language, gracefully fallback
        # to topic list
        return reverse(
            'faq:faq_topic_list',
            kwargs={'slug': self.topic.safe_translation_getter('slug', language_code=lang)}
        )


    def __str__(self):
        default = ugettext('FAQ Question (no translation)')
        return self.safe_translation_getter('text', any_language=True, default=default)

    def save(self, *args, **kwargs):
        super(Question, self).save(*args, **kwargs)
        for lang in self.get_available_languages():
            self.set_current_language(lang)
            if not self.slug and self.name:
                self.slug = slugify(force_text(self.name))
        self.save_translations()

    def is_header(self):
        return self.status == Question.HEADER

    def is_active(self):
        return self.status == Question.ACTIVE
