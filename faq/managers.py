from django.db import models
from django.db.models.query import QuerySet

from aldryn_apphooks_config.managers.parler import (
    AppHookConfigTranslatableManager, AppHookConfigTranslatableQueryset,
)

class QuestionQuerySet(AppHookConfigTranslatableQueryset):
    def active(self):
        """
        Return only "active" (i.e. published) questions.
        """
        return self.filter(status__exact=self.model.ACTIVE)

class QuestionManager(AppHookConfigTranslatableManager):
    def get_query_set(self):
        return QuestionQuerySet(self.model)

    def active(self):
        return self.get_query_set().active()